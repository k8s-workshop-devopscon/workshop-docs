Adding basic functionallity to the cluster
==========================================

The cluster we just created is alredy usable. As we already saw,
some workloads are already successfuly runnig.

The next step will be to add a few necessary plugins to the cluster.
Some of these are officially mainstained by the kubernetes team, some
are external projects with large traction and great usability.

Cluster Metrics API
-------------------

Metrics Server collects resource metrics from Kubelets and exposes
them in Kubernetes apiserver through Metrics API for use by
Horizontal Pod Autoscaler and Vertical Pod Autoscaler.
Metrics API can also be accessed by `kubectl top`, making it
easier to debug autoscaling pipelines.

.. code:: bash

   $ kubectl apply -f https://github.com/kubernetes-sigs/\
       metrics-server/releases/download/v0.4.2/components.yaml

Upon successful installation you should be able to use `kubectl top`::

   $ kubectl get pods -n kube-system -l k8s-app=metrics-server

If you see that the `metrics-server` is not ready or alredy crashed,
check the logs of the pod:

.. code:: bash

   $ kubectl logs -n kube-system -l k8s-app=metrics-server

You will most probably see an error regarding TLS. This is because, the
metrics server uses TLS to connect to the `kubelet` service on the worker
nodes, but it no knowledge on the certificates the kubelet uses.
To fix the issue, check if there are CSR pending from the kubelets:

.. code:: bash

   $ kubectl get csr

   NAME        AGE     SIGNERNAME                      REQUESTOR            CONDITION
   csr-2rn6s   4h50m   kubernetes.io/kubelet-serving   system:node:node-1   Pending
   csr-2rqdj   8h      kubernetes.io/kubelet-serving   system:node:node-2   Pending
   csr-45prg   130m    kubernetes.io/kubelet-serving   system:node:node-2   Pending
   ...

You should approve those with:

.. code:: bash

  $ kubectl certificate approve $(kubectl get csr | \
    grep system:node | grep Pending |  cut -d" " -f 1 | xargs )

Next, restart the metrics server by deleting the Pod:

.. code::

   $ kubectl delete pod -n kube-system -l k8s-app=metrics-server

Check that it is running:

.. code:: bash

   $ kubectl get pod -n kube-system -l k8s-app=metrics-server
   NAME                              READY   STATUS    RESTARTS   AGE
   metrics-server-76f8d9fc69-gpqnn   1/1     Running   0          37s

Now, you should be able to use `kubectl top`:

.. code:: bash

   $ kubectl top nodes
   NAME     CPU(cores)   CPU%   MEMORY(bytes)   MEMORY%
   demo     245m         12%    1806Mi          47%
   node-1   124m         6%     1148Mi          29%
   node-2   132m         6%     1135Mi          29%
   $ kubectl top pods
   No resources found in default namespace.
   $ kubectl top pods -n kube-system
   NAME                                       CPU(cores)   MEMORY(bytes)
   calico-kube-controllers-69496d8b75-8jjk4   1m           11Mi
   calico-node-ghrc9                          31m          50Mi
   calico-node-qq6bn                          31m          50Mi
   calico-node-r9zx8                          44m          48Mi
   coredns-f9fd979d6-6dqbv                    3m           8Mi
   coredns-f9fd979d6-m6h5h                    3m           8Mi
   etcd-demo                                  22m          29Mi
   kube-apiserver-demo                        66m          311Mi
   kube-controller-manager-demo               16m          49Mi
   kube-proxy-jlq52                           1m           11Mi
   kube-proxy-rx9j2                           1m           16Mi
   kube-proxy-s5xvs                           1m           16Mi
   kube-scheduler-demo                        4m           17Mi
   metrics-server-76f8d9fc69-gpqnn            6m           13Mi

User Authentication
-------------------

Kubernetes supports multiple ways of authenticating users. The most basic
is via Certificate Management. However, usually a larger organization would
alredy have a system for authenticating users for other purposes. For example,
Active Directory, LDAP, or be it a Github or Gitlab organizations.

To integrate these with user management in the cluster we deploy
Dex and Dex-Authenticator to the cluster. To ease the installation of these
services we first install the Nginx Ingress controller and Cert Manager.

Nginx Ingress controller
++++++++++++++++++++++++
An ingress controller provides control on how services are exposed
outside the cluster. It can do name based virtual hosting, load balancer,
canary releases, SSL termination and more.

The Nginx Ingress controller is actually one of `many ingress controllers
available <https://kubernetes.io/docs/concepts/services-networking/ingress-controllers/>`_
for Kubernetes. However, it's the only one officially mainstained as a kubernetes add-on
by the kubernetes team.

Unlike other API controllers in Kubernetes this controller isn't included in a
cluster that is created with `kubeadm`.

We install the ingress controller by applying the manifest for a bare metal
ingress from the official website:

.. code:: bash

   $ kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.44.0/deploy/static/provider/baremetal/deploy.yaml

Verify that it all works, by applying the ingress examples::

   $ kubectl apply -f examples/nginx-green-blue-deployment.yml
   $ kubectl apply -f exmaples/blue-green-fan-new-api.yml

Update the `hosts` files of your OS as in `this guide
<https://www.rootusers.com/how-to-use-the-hosts-file-to-fake-dns/>`_
Add entries to for::

   xx.your.server.IP   blue.bar.com
   xx.your.server.IP   green.bar.com

Browse to the blue.bar.com:<nodePort> and verify you see the nginx welcome
page with a blue backround. Repeat the same for the green.bar.com.

Finally, because we don't want to work with URLs with port numbers we add
a simple proxy on the master node, this proxy will forward all TCP to the
HTTP and HTTPS ports of the ingress controller.

Find the correct HTTP and HTTPS ports of the Ingress with:

.. code:: bash

   $ k get svc -n ingress-nginx ingress-nginx -o json | jq '.spec.ports | .[] |{name,nodePort} '
   {
     "name": "http",
     "nodePort": 30337
   }
   {
     "name": "https",
     "nodePort": 31996
   }

Then apply them in::

    # cat << EOF >> /etc/nginx/nginx.conf
    stream {
        upstream nginx_http {
            least_conn;
            server 192.168.3.204:30337 max_fails=3 fail_timeout=5s;
            server 192.168.0.174:30337 max_fails=3 fail_timeout=5s;
            server 192.168.2.14:30337 max_fails=3 fail_timeout=5s;
        }
        server {
            listen 80;
            proxy_pass nginx_http;
            proxy_protocol on;
        }

        upstream nginx_https {
            least_conn;
            server 192.168.3.204:31996 max_fails=3 fail_timeout=5s;
            server 192.168.0.174:31996 max_fails=3 fail_timeout=5s;
            server 192.168.2.14:31996 max_fails=3 fail_timeout=5s;
        }
        server {
            listen     443;
            proxy_pass nginx_https;
            proxy_protocol on;
        }

    }
    EOF

    # nginx -t
    nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
    nginx: configuration file /etc/nginx/nginx.conf test is successful

Before you restart nginx, remove the default site that binds to port 80::

    # rm /etc/nginx/sites-enabled/default

Restart nginx::

    # systemctl restart nginx

The approach taken here is described in the nginx ingress docs as
`self-provisioned edge
<https://kubernetes.github.io/ingress-nginx/deploy/baremetal/#using-a-self-provisioned-edge>`_.

Because we use the proxy protocol, we need to tell nginx to listen to
connections using this protocol. We do this by adding a section in the ingress
configuration map::

    $  k get -o yaml -n ingress-nginx configmaps nginx-configuration
    apiVersion: v1
    data:
       use-proxy-protocol: "true"
    kind: ConfigMap
    ...

To convince our selves that the changes we made let us examine the logs of the
controller. In one shell session we exectue::

   $ k -f logs -l app.kubernetes.io/name=ingress-nginx -n ingress-nginx

and in another we execute a command to find our IP address::

   $ curl "http://myexternalip.com/raw"

Following with a request to a virtual host::

   $ curl http://green-nginx.tiram.it

The logs should show the original IP address::

   95.114.118.102 - [95.114.118.102] - - [06/Apr/2021:16:58:21 +0000] "GET / HTTP/2.0" 200 639 "-" "curl/7.74.0" 35 0.006 [default-nginx-green-80] 10.233.210.130:80 639 0.008 200 6529e235cca7243e6782bfcafa9bd4c2

This format is explained in the `default logging configuration
<https://kubernetes.github.io/ingress-nginx/user-guide/nginx-configuration/log-format/>`_.

This setting will also inject variables to preserve the original IP for your
applications. Check the configuration of the Virtual Host by running:

.. code:: bash

  $ k exec -it -n ingress-nginx nginx-ingress-controller-d8444ccfd-h4vd5 -- cat /etc/nginx/nginx.conf
  ...
  ## start server green-nginx.tiram.it
  server {
      server_name green-nginx.tiram.it ;

      listen 80 proxy_protocol;

      set $proxy_upstream_name "-";
      set $pass_access_scheme $scheme;
      set $pass_server_port $server_port;
      set $best_http_host $http_host;
      set $pass_port $pass_server_port;
      ...
      proxy_set_header X-Real-IP              $remote_addr;
      proxy_set_header X-Forwarded-For        $remote_addr;
      ...
  }

We can see the ``X-Forwarded-For`` header in the logs of the nginx pod::

  $ k exec -it nginx-green-7fc6685bf7-c26nx -- grep -C 3 log_format /etc/nginx/nginx.conf
    include       /etc/nginx/mime.types;
    default_type  application/octet-stream;

    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';


And in the logs::

  $ k logs nginx-green-7fc6685bf7-c26nx | grep Firefox
  10.233.210.141 - - [06/Apr/2021:19:47:03 +0000] "GET / HTTP/1.1" 200 639 "-" "Mozilla/5.0 (X11; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0" "77.7.73.199"
  10.233.210.141 - - [06/Apr/2021:19:47:03 +0000] "GET /favicon.ico HTTP/1.1" 404 153 "http://green-nginx.tiram.it/" "Mozilla/5.0 (X11; Linux x86_64; rv:85.0) Gecko/20100101 Firefox/85.0" "77.7.73.199"

Pitfalls
^^^^^^^^

If your frond end shows error 503 in the browser (Service temporarily
unavailable), check the Ingress resource for errors::

   $  k describe ingress red-nginx
   Name:             red-nginx
   Namespace:        default
   Address:
   Default backend:  default-http-backend:80 (<error: endpoints "default-http-backend" not found>)
   TLS:
     red.alt-shift.digital terminates red-nginx.alt-shift.digital
   Rules:
     Host                         Path  Backends
     ----                         ----  --------
     red-nginx.alt-shift.digital
                                  /   red-green:80 (<error: endpoints "red-green" not found>)
   Annotations:                   cert-manager.io/cluster-issuer: letsencrypt-staging
                                  kubernetes.io/ingress.class: nginx
   Events:
     Type    Reason  Age        From                      Message
     ----    ------  ----       ----                      -------
     Normal  CREATE  <invalid>  nginx-ingress-controller  Ingress default/red-nginx


cert-manager
++++++++++++

cert-manager helps us secure services in the cluster using certificates
from Let's Encrypt (it can also manage other CAs). It's easy to install
and use. We install it with:

.. code:: bash

   $ kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.2.0/cert-manager.yaml

Before we can start using certificates from LE, we need to create a
``certificateissuer`` object. This objects can be namespaced
(``issuers.cert-manager.io``) cluster wide available
(``clusterissuers.cert-manager.io``). We will create a cluster wide issuer
which will later use for deploying Dex:

.. code:: bash

   cat << EOF > production-cluster-issuer.yml
   apiVersion: cert-manager.io/v1
   kind: ClusterIssuer
   metadata:
     name: letsencrypt-production
   spec:
     acme:
       # The ACME server URL
       server: https://acme-v02.api.letsencrypt.org/directory
       # Email address used for ACME registration
       email: <YOUR EMAIL>
       # Name of a secret used to store the ACME account private key
       privateKeySecretRef:
         name: letsencrypt-production
       # Enable the HTTP-01 challenge provider
       solvers:
       - http01:
           ingress:
             class: nginx
    EOF
    cat << EOF > staging-issuer.yml
    apiVersion: cert-manager.io/v1
    kind: ClusterIssuer
    metadata:
      name: letsencrypt-staging
    spec:
      acme:
        # The ACME server URL
        server: https://acme-staging-v02.api.letsencrypt.org/directory
        # Email address used for ACME registration
        email: <YOUR EMAIL>
        # Name of a secret used to store the ACME account private key
        privateKeySecretRef:
          name: letsencrypt-staging
        # Enable the HTTP-01 challenge provider
        solvers:
        - http01:
            ingress:
              class: nginx
      EOF

      $ kubectl apply -f production-cluster-issuer.yml
      $ kubectl apply -f staging-issuer.yml

That is it. We can now create ingress with TLS. For example we can secure the
green-blue nginx deployment with the following changes to the ingress:


.. code:: yaml

   apiVersion: networking.k8s.io/v1
   kind: Ingress
   metadata:
     name: green-vhost
     annotations:
       kubernetes.io/ingress.class: nginx
       cert-manager.io/cluster-issuer: letsencrypt-production
   spec:
     tls:
     - hosts:
       - green-nginx.tiram.it
       secretName: green-nginx.tiram.it
     rules:
     - host: green-nginx.tiram.it
       http:
         paths:
         - pathType: Prefix
           path: /
           backend:
             service:
               name: nginx-green
               port:
                 number: 80
   ---
   apiVersion: networking.k8s.io/v1
   kind: Ingress
   metadata:
     name: blue-vhost
     annotations:
       kubernetes.io/ingress.class: nginx
       cert-manager.io/cluster-issuer: letsencrypt-production
   spec:
     tls:
     - hosts:
       - blue-nginx.tiram.it
       secretName: blue-nginx.tiram.it
     rules:
     - host: blue-nginx.tiram.it
       http:
         paths:
         - pathType: Prefix
           path: /
           backend:
             service:
               name: nginx-blue
               port:
                 number: 80

Limits
^^^^^^

If you issue too many certificates you will encounter the weekly rate limit.
And you will see in the logs an error::

   $ k describe certificate ...
   ...
   Events:
     Type     Reason     Age                 From          Message
     ----     ------     ----                ----          -------
     Normal   Issuing    47m (x13 over 12h)  cert-manager  Issuing certificate as Secret does not exist
     Normal   Generated  47m                 cert-manager  Stored new private key in temporary Secret resource "vhost-tls-b5fpb"
     Warning  Failed     47m                 cert-manager  The certificate request has failed to complete and will be retried: Failed to wait for order resource "vhost-tls-xtfxs-233725225" to become ready: order is in "errored" state: Failed to create Order: 429 urn:ietf:params:acme:error:rateLimited: Error creating new order :: too many certificates already issued for exact set of domains: xxxx: see https://letsencrypt.org/docs/rate-limits/

The documentation of letsencrypt.org offers a fairly simple way to avoid it.
Create SAN certificates.
In the example above we can define just one Ingress resource with multiple
rules and a certificate for multiple hosts::

   $ cat examples/secure-nginx-ingress.yml
   apiVersion: networking.k8s.io/v1
   kind: Ingress
   metadata:
     name: green-nginx
     annotations:
       kubernetes.io/ingress.class: nginx
       nginx.ingress.kubernetes.io/backend-protocol: "HTTPS"
       nginx.ingress.kubernetes.io/ssl-passthrough: "true"
       cert-manager.io/cluster-issuer: letsencrypt-staging
   spec:
     tls:
     - hosts:
       - green-nginx.tiram.it
       - blue-nginx.tiram.it
       secretName: green-blue-nginx.tiram.it
     rules:
     - host: green-nginx.tiram.it
       http:
         paths:
         - pathType: Exact
           path: /
           backend:
             service:
               name: nginx-green
               port:
                 number: 80
     - host: blue-nginx.tiram.it
       http:
         paths:
         - pathType: Exact
           path: /
           backend:
             service:
               name: blue-nginx
               port:
                 number: 80

You can also `explicitly create certificates <https://cert-manager.io/docs/tutorials/acme/http-validation/>`_
without annotations in the Ingress manifests and re-use those certificates
in the Ingress manifest.

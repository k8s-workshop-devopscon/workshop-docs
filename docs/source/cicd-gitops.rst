CI\CD and GitOps
================

So far we have built a cluster that allows multiple teams to work together
towards shipping a working software stack without tripping on each other's
toes.
However, we are at a situation where every team member can do everything in
the namespace that belongs to his team, also there we have already deployed
workloads with multiple problems (as the polaris dashboard shows us).


Testing and Vetting our Software
--------------------------------

First, let's decide what we want to test ... for our simple colorful Nginx
Software, it's not hard to imagine what we test. We want to assert that
the page we display has the correct CSS class.

We will use Gitlab's Pipeline and deploy our Nginx container in the frontend
namespace and therefore we will use the frontend subgroup.


After creating the repository on gitlab, we clone it locally and create
a basic directory structure::

   $ mkdir k8s
   $ touch Makefile Dockerfile .gitignore .dockerignore .gitlab.yml

Note, I like working with ``Makefile`` because I don't like (or can't even
remember all the commands and correct flags required for building modern
software. Also, I want to run all my tests locally before pushing my code
and triggering a "build & deploy" pipeline.

Testing locally with Makefile
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``make`` is an old and trusted companion for many software projects.
Originally, intended as a build tool, it can also be used to execute any task.
It has some defaults we can override, to make our lives easier.

First, we override the default shell and target by adding at the top::

   ###
   #  general configuration of the makefile itself
   ###
   SHELL := /bin/bash
   .DEFAULT_GOAL := help

``make`` will execute the first target it finds at the top of the file, if we
don't specify a target. We use that to create a beautiful ``help`` menu for
onboarding new team members and contributors to the project. Also, we add
a ``PHONY`` target, which will be executed even if there is a file called test
locally::

   .PHONY: test

Finally, we add a small Python program to
`generate a help menu <https://github.com/oz123/help-make-helper>`_ for
We also add a target to generate this help::

   help:
        @$(PY) make-help-helper.py < $(MAKEFILE_LIST)

Finaly we add a test target with some parameters we can override::

   test: HOST ?= localhost#? what host is running NGinx
   test: PORT := 8080#? the port which accepts connections
   test: COLOR ?= green#? which color should be the backround
   test: ## test that the HOST shows background COLOR
        curl -s -k -L $(HOST):$(PORT) | grep -qc "background: $(COLOR)"

Now we can run our first test, which will obviously fail::

   $ make test
   curl -s -k -L localhost  | grep -qc "background: green "
   make: *** [Makefile:86: test] Error 1

That is because our localhost isn't necessarily running Nginx. Let's fix it
by creating a proper docker image and launching it.


Add the following `Dockerfile` and `welcome-green.html` to the repository::

   $ cat Dockerfile
   FROM docker.io/nginx

   COPY welcome-green.html /usr/share/nginx/html/index.html

   $ cat welcome-green.html
   <!DOCTYPE html>
   <html>
   <head>
   <title>Welcome to nginx!</title>
   <style>
       body {
           width: 35em;
           margin: 0 auto;
           font-family: Tahoma, Verdana, Arial, sans-serif;
           background: green;
       }
   </style>
   </head>
   <body>
   <h1>Welcome to nginx!</h1>
   <p>If you see this page, the nginx web server is successfully installed and
   working. Further configuration is required.</p>

   <p>For online documentation and support please refer to
   <a href="http://nginx.org/">nginx.org</a>.<br/>
   Commercial support is available at
   <a href="http://nginx.com/">nginx.com</a>.</p>

   <p><em>Thank you for using nginx.</em></p>
   </body>
   </html>

Now add the appropriate targets to build and run the container locally::


   docker-build::  ## build a docker image
           docker build $(OPTS) -t $(REGISTRY)/$(ORG)/$(IMG):v$(VERSION) -f Dockerfile .

   docker-push::  ## push docker image to $(REGISTRY)
           docker push $(REGISTRY)/$(ORG)/$(IMG):v$(VERSION)

   docker-run:: OPTS =? "--rm -p 8080:80 -d --name $(IMG)-$(VERSION)"
   docker-run::  ## run the image and expose port 80 over port 8080 on localhost
           docker run $(OPTS) $(REGISTRY)/$(ORG)/$(IMG):v$(VERSION)
           sleep 2; # add this so curl after run does not fail, nginx needs a while to start

Now we can add a simple integration test to our glorious Nginx with green
background::


   integration-test:: docker-clean docker-build docker-run test docker-clean

   docker-clean::
        if docker ps | grep $(subst -,_,$(IMG))_$(VERSION); then docker stop $(subst -,_,$(IMG))_$(VERSION); fi

This should work locally. Because our test is parameterized, We can also easily
test the "Bad Path", we modify the original test to be a target depending on
two other steps::

   test-all:
         $(MAKE) test-color
         $(MAKE) test-color COLOR=red || echo "OK! The color is not red"

Running the integration test::

   $ make integration-test
   if docker ps | grep front_nginx_b0f13b9; then docker stop front_nginx_b0f13b9; fi
   df3fb4b3f3bf        oz123/front-nginx:vb0f13b9   "/docker-entrypoint.…"   39 seconds ago      Up 38 seconds       0.0.0.0:8080->80/tcp   front_nginx_b0f13b9
   front_nginx_b0f13b9
   docker build  -t docker.io/oz123/front-nginx:vb0f13b9 -f Dockerfile .
   Sending build context to Docker daemon  100.9kB
   Step 1/2 : FROM docker.io/nginx
    ---> 62d49f9bab67
   Step 2/2 : COPY welcome-green.html /usr/share/nginx/html/index.html
    ---> Using cache
    ---> 34d07ef040c0
   Successfully built 34d07ef040c0
   Successfully tagged oz123/front-nginx:vb0f13b9
   docker run --rm -p 8080:80 -d --name front_nginx_b0f13b9 docker.io/oz123/front-nginx:vb0f13b9
   5ff3ca7ae34e372a07b36696378935c2a60c7b036d2bce1b4dea0ffa3316f839
   sleep 2; # add this so curl after run does not fail, nginx needs a while to start
   make test-color
   make[1]: Entering directory '/home/oznt/Software/k8s-workshop-london/front-nginx'
   curl -s -k -L localhost:8080 | grep -qc "background: green"
   make[1]: Leaving directory '/home/oznt/Software/k8s-workshop-london/front-nginx'
   make test-color COLOR=red || echo "OK! The color is not red"
   make[1]: Entering directory '/home/oznt/Software/k8s-workshop-london/front-nginx'
   curl -s -k -L localhost:8080 | grep -qc "background: red"
   make[1]: *** [Makefile:91: test-color] Error 1
   make[1]: Leaving directory '/home/oznt/Software/k8s-workshop-london/front-nginx'
   OK! The color is not red

Now, we are ready to build a pipeline in Gitlab.

CI\CD with Gitlab
-----------------

I already set up a few workers for the whole group. The workers are registered
and configured with::

  $   /usr/bin/gitlab-runner \
      register \
       --non-interactive \
       --name "${WORKER}" \
          --url "https://gitlab.com/" \
          --registration-token "${RUNNER_TOKEN}" \
          --executor "docker" \
          --docker-privileged \
          --docker-image "docker:19.03" \
          --docker-volumes "/var/run/docker.sock:/var/run/docker.sock"
          --tag-list "docker,privileged" \
          --run-untagged \
          --locked

The resulting config file should like like this::

  # cat /etc/gitlab-runner/config.toml
  concurrent = 1
  check_interval = 0

  [session_server]
    session_timeout = 1800

  [[runners]]
    name = "devops-con-worker"
    url = "https://gitlab.com/"
    token = "__redacted__"
    executor = "docker"
    [runners.custom_build_dir]
    [runners.cache]
      Insecure = false
      [runners.cache.s3]
      [runners.cache.gcs]
      [runners.cache.azure]
    [runners.docker]
      tls_verify = false
      image = "docker:19.03"
      privileged = true
      disable_entrypoint_overwrite = false
      oom_kill_disable = false
      disable_cache = false
      volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
      shm_size = 0

You might want to use your own CI\CD system. Using a
`Makefile`, we can totally avoid writing complex Pipeline in a DSL or shell
embedded in YAML. Note however, that you can `run gitlab jobs locally <https://der-jd.de/blog/2021/01/28/Run-gitlab-ci-jobs-on-your-workstation/>`_.

We begin by definning three stages in `.gitlab.yml`:

.. code:: yaml

   # Docker image based on docker and includes:
   #  - GNU Make installed
   #  - git
   #  - bash
   image: docker.io/oz123/devopscon-builder

   stages:
     - build
     - test

   build:
     stage: build
     script:
     - make docker-build

   test:
     stage: test
     script:
     - make docker-run
     - make docker-test

   clean:
   stage: clean
   script:
    - make docker-clean
   rules:
   - when: always

Similar to the integration test we have in the `Makefile` we have to clean the
running image, so we can run the test multiple times. We can force a stage to
always run in gitlab, even if the previous stage failed.
Note, however, that we slightly have to change the test stage. In the local test
we test that Nginx serves the correct page by accessing it from outside the
docker container. In gitlab, we run the tests by creating a new container which
is not linked with the docker container which is responsible of running the
test. Hence, doing `curl localhost` will fail. Executing the `curl` inside the
dockcer conatiner that runs NGinx still tests that Nginx serve the correct file.
We don't need to test outside docker, since we eventually deloy to kubernetes.

So, now we have a working container, which can be deployed to the cluster.
Before we deploy the container, we can add a few more tests to guarantee we have
good software.

We can add as many steps to vet the software, for example - code style checking,
or `container scanning <https://github.com/hadolint/hadolint>`_::

   $ make docker-lint
   docker run --rm -i hadolint/hadolint < Dockerfile
   Unable to find image 'hadolint/hadolint:latest' locally
   latest: Pulling from hadolint/hadolint
   f061d454f745: Pull complete
   Digest: sha256:d6ee991347e4e0bd38e12b6bf915ac01c757e92e7da52c7a51a39e723ee7f68d
   Status: Downloaded newer image for hadolint/hadolint:latest
   -:1 DL3006 warning: Always tag the version of an image explicitly
   make: *** [Makefile:114: docker-lint] Error 1


For example, we can also add `pylint` and `go vet` to check the code style of
Python and Go in our projects:

.. code:: yaml

  stages:
  - linting
  - build
  - test
  - clean

  go-vet:
    stage: linting
    script:
      - make go-vet

  pylint:
    stage: linting
    script:
      - make pylint

  docker-scan:
    stage: linting
    script:
      - make docker-lint

CVE Scanning with Trivy
^^^^^^^^^^^^^^^^^^^^^^^

Besides static analysis and linting, we can also add a more rigorous
check to what gets deployed into the cluster. One such check, is to guarantee
that we do not deploy images with known CVEs in them.

`Trivy <https://github.com/aquasecurity/trivy>`_ scan each images and checks
the software packages in it for known CVEs.

First, we add a target in our `Makefile`::

   docker-cve-scan:: SEVERITY := CRITICAL#? which severity level to consider
   docker-cve-scan:: ## scan docker images for known CVEs
        trivy image --exit-code 1 --severity $(SEVERITY) $(REGISTRY)/$(ORG)/$(IMG):v$(VERSION

We can then add it to `.gitlab-ci.yml` after the build stage:

.. code:: yaml

  stages:
    - linting
    - build
    - cve-scan
    - test
    - clean
  ...
  build:
    stage: build
    script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - make docker-build REGISTRY=$CI_REGISTRY ORG=$CI_PROJECT_PATH
    - make docker-push REGISTRY=$CI_REGISTRY ORG=$CI_PROJECT_PATH

  trivy-scan:
    stage: cve-scan
    allow_failure: false
    script:
    - make docker-cve-scan REGISTRY=$CI_REGISTRY ORG=$CI_PROJECT_PATH


If the CVE scan is successful, we can continue to run the tests and push the
image to Gitlab's built-in container registry.

Pushing the Image to Gitlab's container registry
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Gitlab's jobs run with a plethora of usufull variables that we can use. Every,
job is stared with an enviroment variable containing a short-lived API token
which allows us to push a container image to the registry. We use this token,
and some other variables (alredy shown before) to properly tag and push the
image:

.. code:: yaml

  stages:
    ...
    - cve-scan
    - test
    - push
    - clean

  build:
    stage: build
    script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - make docker-push REGISTRY=$CI_REGISTRY ORG=$CI_PROJECT_PATH

Deploying to kubernetes
^^^^^^^^^^^^^^^^^^^^^^^

We begin by creating a kustomize template to update the delpoyment. This can
be replaced with Helm or what ever mechanism you like.

First, we need to pull the image we built from Gitlab's Container registry.
Therefore, we need to create a deployment token:

.. image:: _static/img/deploy-token.png

We can verify this token works by testing it locally::

   $ docker login -u gitlab-deploy-frontend -p xxxredactedxxx https://registry.gitlab.com
   WARNING! Using --password via the CLI is insecure. Use --password-stdin.
   WARNING! Your password will be stored unencrypted in /home/oznt/.docker/config.json.
   Configure a credential helper to remove this warning. See
   https://docs.docker.com/engine/reference/commandline/login/#credentials-store

   Login Succeeded
   $ docker pull registry.gitlab.com/k8s-workshop-devopscon/frontend/front-nginx/front-nginx:v8f7b817
   v8f7b817: Pulling from k8s-workshop-devopscon/frontend/front-nginx/front-nginx
   f7ec5a41d630: Already exists
   aa1efa14b3bf: Already exists
   b78b95af9b17: Already exists
   c7d6bca2b8dc: Already exists
   cf16cd8e71e0: Already exists
   0241c68333ef: Already exists
   1040b750744b: Pull complete
   127eec9eeefb: Pull complete
   Digest: sha256:1bf5389b7feb12bfe2de77e731fb13fe99db30bb337691cdf209e5fffbf1af8c
   Status: Downloaded newer image for registry.gitlab.com/k8s-workshop-devopscon/frontend/front-nginx/front-nginx:v8f7b817
   registry.gitlab.com/k8s-workshop-devopscon/frontend/front-nginx/front-nginx:v8f7b817

You can find the correct image name to pull in the UI of the container
registry:

.. image:: _static/img/registry.png

We use the username and the token created for us to create a docker pull
secret::

   $ k create secret docker-registry --docker-password XXXredactedXXX \
       --docker-username gitlab-deploy-frontend \
       --docker-server=https://registry.gitlab.com -n front gitlab-pull-secret
   secret/gitlab-pull-secret created


Create a deployment file with the correct image name in gitlab and pull secret:

.. code:: yaml

   apiVersion: apps/v1
   kind: Deployment
   metadata:
     name: nginx-green
     namespace: front
     labels:
       app: nginx-green
   spec:
     replicas: 1
     selector:
       matchLabels:
         app: nginx-green
     template:
       metadata:
         labels:
           app: nginx-green
       spec:
         containers:
         - name: nginx
           image: registry.gitlab.com/k8s-workshop-devopscon/frontend/front-nginx/front-nginx:v8f7b817
           ports:
           - containerPort: 80
         imagePullSecrets:
         - name: gitlab-pull-secret

Check that this works::

   $ k apply -f front-nginx/k8s/deployment.yml
   deployment.apps/nginx-green created

   $ k get pods -n front
   NAME                          READY   STATUS    RESTARTS   AGE
   nginx-green-65446d697-9jq6p   1/1     Running   0          13s

Also, verify that the container uses the correct image::

   $ k describe pods -n front -l app=nginx-green | grep Image
     Image:          registry.gitlab.com/k8s-workshop-devopscon/frontend/front-nginx/front-nginx:latest
     Image ID:
        Reason:       ImagePullBackOff
    Warning  Failed     9s (x5 over 88s)         kubelet            Error: ImagePullBackOff
    Warning  Failed     <invalid> (x4 over 89s)  kubelet            Error: ErrImagePull
      Image:          registry.gitlab.com/k8s-workshop-devopscon/frontend/front-nginx/front-nginx:v8f7b817
      Image ID:       docker-pullable://registry.gitlab.com/k8s-workshop-devopscon/frontend/front-nginx/front-nginx@sha256:1bf5389b7feb12bfe2de77e731fb13fe99db30bb337691cdf209e5fffbf1af8c

We can now clean this and process to creating the `kustomizatio.yml`::

   $ k delete -f k8s/deployment.yml

The kustomization command compiles a direcotry of resources to a valid set
of manifests:

.. code:: yaml

  apiVersion: kustomize.config.k8s.io/v1beta1
  kind: Kustomization
  resources:
  - deployment.yml
  - ingress.yml
  - service.yml
  images:
  - name: registry/no-such-image:no-such-label
    newName: registry.gitlab.com/k8s-workshop-devopscon/frontend/front-nginx/front-nginx
    newTag: LATEST
  patches:
  - patch.yml


We use it to set the correct image name in the deployment and update the
environment of the nginx container using the pach directive:

.. code:: yaml

   apiVersion: apps/v1
   kind: Deployment
   metadata:
     name: nginx-green
     namespace: front
   spec:
     template:
       spec:
         containers:
         - name: nginx
           env:
           - name: APP_VERSION
             value: SSVERSIONSS

Note that the values are set with intent to something we can identify as
invalid, e.g. ``SSVERSIONSS`` or ``registry/no-such-image:no-such-label``.
This is to prevent using these resources directly. We use these resources
with the command ``k apply -k k8s/``. To see what will be applied you can
use ``k kustomize k8s``. We now add steps in ``.gitalb-ci.yml`` to update
the ``kustomization`` and patch files.


.. code:: yaml

   stages:
   ...
   push
   clean
   deploy

   build:
   ...

   deploy:
    stage: deploy
    script:
    - make k8s-patch
    - make k8s-kustomize
    - make k8s-deploy

Now, we have all the targets in `.gitlab-ci.yml` and we add the following
to the `Makefile`::

   k8s-patch:: ## update kustomization and patch with the correct versions
        sed -i "s/LATEST/v$(VERSION)/g" k8s/kustomization.yml
        sed -i "s/SSVERSIONSS/v$(VERSION)/g" k8s/patch.yml

   k8s-kustomize:: # show what will be deployed
        kubectl $(OPTS) kustomize k8s/

   k8s-deploy: ## deploy latest image to k8s
        kubectl $(OPTS) apply -k k8s/

We can almost deploy to our cluster. If we trigger the pipeline now, it will
fail, because `kubectl` will not know with which cluster to communicate and
with which credentials it should log in. To deploy to the cluster we create a
service account with limited permissions which will be used to deploy from
github::

   $ k create serviceaccount -n front sa-front-deployer
   serviceaccount/sa-front-deployer created

This will create a secret token which can be used to authenticate to the
cluster. The token alone is not enough. We need to create a ``Role`` and
``RoleBinding`` for this account which allow it to make changes to the
namespace. We can base these on the previously demonstrated ``backed-1``
developer role::

   kind: Role
   apiVersion: rbac.authorization.k8s.io/v1
   metadata:
     name: sa-deployer-front
     namespace: front
   rules:
   - apiGroups: [""]
     resources: ["pods/log", "events"]
     verbs: ["get", "list"]
   - apiGroups: [""]
     resources: ["pods", "replicationcontrollers"]
     verbs: ["get", "list", "watch", "create", "update", "patch"]
   - apiGroups: [""]
     resources: ["pods/exec"]
     verbs: ["create"]
   - apiGroups: [""]
     resources: ["configmaps", "secrets", "services"]
     verbs: ["get", "list", "watch", "create", "update", "patch"]
   - apiGroups: ["extensions", "apps"]
     resources: ["deployments", "statefulsets"]
     verbs: ["get", "list", "watch", "create", "update", "patch"]
   - apiGroups: ["apps"]
     resources: ["replicasets"]
     verbs: ["get", "list", "update", "patch", "create", "watch"]
   - apiGroups: ["networking.k8s.io", "extensions"]
     resources: ["ingresses", "ingressclasses"]
     verbs: ["get", "list", "watch", "create", "update", "patch"]
   ---
   kind: RoleBinding
   apiVersion: rbac.authorization.k8s.io/v1beta1
   metadata:
     name: sa-deployer-front
     namespace: front
   subjects:
  - kind: ServiceAccount
    name: sa-front-deployer
    namespace: front
   roleRef:
     kind: Role
     name: sa-deployed-front
     apiGroup: rbac.authorization.k8s.io

To test this all working, get the token of the service account and add it to
your ``KUBECONFIG``:

.. code:: bash

    $ TOKEN=$(k get secrets -n front \
      -o=jsonpath='{.items[?(@..metadata.annotations.kubernetes\.io/service-account\.name=="sa-front-deployer")].data.token}')
    $ kubectl config set-credentials sa-front-deployer --token=$(echo $TOKEN | base64 -d)

This should work in the ``front`` but not in the default ``namespace``:

.. code:: bash

   $ k access-matrix -n front --user=sa-front-deployer | grep  ✔
   configmaps                                      ✔     ✔       ✔       ✖
   deployments.apps                                ✔     ✔       ✔       ✖
   events                                          ✔     ✖       ✖       ✖
   ingresses.extensions                            ✔     ✔       ✔       ✖
   ingresses.networking.k8s.io                     ✔     ✔       ✔       ✖
   pods                                            ✔     ✔       ✔       ✖
   replicasets.apps                                ✔     ✔       ✔       ✖
   replicationcontrollers                          ✔     ✔       ✔       ✖
   secrets                                         ✔     ✔       ✔       ✖
   services                                        ✔     ✔       ✔       ✖
   statefulsets.apps                               ✔     ✔       ✔       ✖

   $ k get pods  --user=sa-front-deployer
   Error from server (Forbidden): pods is forbidden: User "system:serviceaccount:front:sa-front-deployer" cannot list resource "pods" in API group "" in the namespace "default"

To use this user in Gitlab we need to create a `kubeconfig` file that we can
feed at run time. To do this, we need to distil the login information:

.. code:: bash

   $ kubectl config set-context sa-front-deployer --cluster=alt-shift.digital --namespace=front --user=sa-front-deployer
   $ kubectl config use-context sa-frontend-deployer
   $ kubectl config view --minify
   apiVersion: v1
   clusters:
   - cluster:
       certificate-authority-data: DATA+OMITTED
       server: https://213.95.154.199:6443
     name: alt-shift.digital
   contexts:
   - context:
       cluster: alt-shift.digital
       namespace: front
       user: sa-front-deployer
     name: sa-front-deployer
   current-context: sa-front-deployer
   kind: Config
   preferences: {}
   users:
   - name: sa-front-deployer
     user:
       token: REDACTED

To use this in gitlab, we encode the whole file in `base64` with the
raw information::

   $ kubectl config view --minify --ra
   .... copy the file to your clipboard ...

Then, add it as a variable of type `File` in the CI\CD configuration:

.. image:: _static/img/ci-cd-setting.png
.. image:: _static/img/file-variable.png

Now, ``kubectl`` will be able to communicate with the cluster. With the
injection of the ``APP_VERSION`` to `/version` path we can get the correct
application version::

   $ curl -k https://front.alt-shift.digital/version
   {"APP_VERSION":v938b4cb}

With this we have a CI\CD pipeline to deploy the application to the cluster
with every chagne. Currenlty we trigger this deployment on every branch.
We add one more final change, to deploy the software only when we release
specific tags or simply push to the master branch:

.. code:: yaml

  deploy:
    stage: deploy
    script:
    - ls -l $KUBECONFIG
    - kubectl get pods -n front
    - kubectl get pods
    - kubectl get pods -n front --kubeconfig=$KUBECONFIG
    - make k8s-patch
    - make k8s-deploy
    only:
     - tags
     - master

With that our CI\CD is done.

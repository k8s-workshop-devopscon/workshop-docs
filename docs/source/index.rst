.. k8s workshop london documentation master file, created by
   sphinx-quickstart on Wed Mar 24 10:01:03 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Devops London k8s workshop
=====================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   bootstrap
   basic-addons
   multiuser
   monitoring
   limits-auditing
   cicd-gitops

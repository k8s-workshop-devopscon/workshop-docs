Mutliuser
=========

In order to allow multiple users to use our cluster we will deploy Dex and
Dex-K8S-Authenticator and configure Dex to use a github organization as
the source of truth of the cluster users. Using a RBAC we can then allow
different user groups different actions in different namespaces or even the
whole cluster.

Deploying Dex and dex-k8s-authenticator
---------------------------------------

Dex is communicating back and forth with Github (or your Auth provider of
choice). In order to secure this communication over HTTPS we need to use a
registered domain.

Registering Dex as an OAuth App
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Configuring with github
+++++++++++++++++++++++

We begin by registring a new OAuth application in Github organization's page:

Go to the Github organization page:

.. image:: _static/img/orga.png

Switch to your oragnization's page:

.. image:: _static/img/switch.png

Under the developer settings find the button for OAuth apps:

.. image:: _static/img/devsettings.png

In the OAuth Apps page click on the New OAuth app:

.. image:: _static/img/new-oauth.png

Fill in the details:

.. image:: _static/img/register-app.png

Finally, create a new secret for this app:

.. image:: _static/img/create-new-client-secret.png

Copy this secret and client ID to a safe password manager for later.

Configuring with gitlab
+++++++++++++++++++++++

This is much easier in Gitlab!

Just browser to your `Group Settings and add an application <https://gitlab.com/groups/k8s-workshop-devopscon/-/settings/applications>`_.

The application should be able to read the following scopes: `read_user` and
`openid`.

.. image:: _static/img/dex-gitlab.png

Configuring ``kube-api-server``
+++++++++++++++++++++++++++++++

On the master node edit the file configuration for kubeadm and add the
following lines to the section ``apiServer.extraArgs``::

     oidc-client-id: dex-k8s-authenticator
     oidc-groups-claim: groups
     oidc-issuer-url: https://dex.xmo.tiram.it/
     oidc-username-claim: email

After that, regenerate the manifest for kube-api-server by running::

    # kubeadm init phase control-plane apiserver --config kubeadm-xmo-master-1.yaml

Verify that the kube-api-server is running with::

    # docker ps | grep api | awk '//{print $2, $3, $4, $5, $6}'
    9ba91a90b7d1 "kube-apiserver --ad…" 8 seconds
    k8s.gcr.io/pause:3.2 "/pause" 8 seconds


You are now ready for actually deploying Dex to the cluter.


As already mentioned, Dex is a helper application to connect cluster users to
differnt identity provider. The source code repository contains a small
application which is used as a front end for dex and for catching the redirect
after the authentication is complete.
This application has a few 'forks' making some additional UI improvements.
`dex-k8s-authenticator` is one of those. Upon redirection, the user lands on a
page which shows him how to configure his `kubeconfig` file for using the
`kubectl` with the authentication token provided by dex.

Begin by examining the manifest for Dex in ``example/dex/dex.yml``.
The manifest is not complete and you will have to feel the missing URLs, Client
IDs and Client Secret you created earlier (the only sections you need to edit
are in Secret and ConfigMap at the bottom of the file).

After adding the missing information in the manifest, apply the manifest with::

   $ k apply -f examples/dex/dex.yml
   namespace/dex created
   configmap/dex created
   deployment.apps/dex created
   service/dex created
   ingress.networking.k8s.io/dex created
   serviceaccount/dex created
   role.rbac.authorization.k8s.io/dex created
   rolebinding.rbac.authorization.k8s.io/dex created
   clusterrole.rbac.authorization.k8s.io/dex created
   clusterrolebinding.rbac.authorization.k8s.io/dex created
   secret/dex created

You can now proceed to adding the missing values in ``ConfigMap`` at the bottom
of ``examples/dex/dex-k8s-authenticator``. Once completed, apply the manifest
with::

   $ k apply -f examples/dex/dex-k8s-authenticator.yml
   configmap/dex-k8s-authenticator created
   deployment.apps/dex-k8s-authenticator created
   service/dex-k8s-authenticator created
   ingress.networking.k8s.io/dex-k8s-authenticator created

Once all pods are running, browse to the login URL:

.. image:: _static/img/login-page.png

Click on Login with Github:

.. image:: _static/img/auth-github.png

Once the login is successful you will see a page instructing you how to
configure `kubectl` to access the cluster:

.. image:: _static/img/login-success.png

Pitfalls:
^^^^^^^^^

If you see errors about non matcher issue in the browser, check the logs with::


  $ k logs -n dex dex-k8s-authenticator-58dbd48f89-xw8f2
  2021/04/08 20:50:21 Using config file: /app/config.yaml
  2021/04/08 20:50:21 Creating new provider https://dex.xmo.tiram.it
  2021/04/08 20:50:21 Failed to query provider "https://dex.xmo.tiram.it": oidc: issuer did not match the issuer returned by provider, expected "https://dex.xmo.tiram.it" got "https://dex.xmo.tiram.it/"

If you see error 400 in browser::

   $ k logs -n dex dex-k8s-authenticator-58dbd48f89-jdq9r
   2021/04/08 20:57:03 Redirecting post-loginto: https://dex.xmo.tiram.it/auth?access_type=offline&client_id=dex-k8s-authenticator&redirect_uri=https%3A%2F%2Flogin.xmo.tiram.it%2Fcallback%2F&response_type=code&scope=openid+profile+email+offline_access+groups&state=Vgn2lp5QnymFtLntKX5dM8k773PwcM87T4hQtiESC1q8wkUBgw5D3kH0r5qJ
   2021/04/08 21:00:08 Handling callback for: xmo-cluster
   2021/04/08 21:00:08 handleCallback: request error. error: redirect_uri_mismatch, error_description: The redirect_uri MUST match the registered callback URL for this application.

Role Based Access Control
-------------------------

Now, every users in the oragnization's group specified in the Dex configuration
file can access the cluster. However, ``kube-api-server`` will only use the
information from Github to decide whether a request is legitimate or not.
In order to access or modify anything in the cluster a username or a group
name has to have explicit permission granted. This permission is explicitly
given with a system of ``Roles`` and ``RoleBindings`` enabled via the RBAC
system.

We begin by creating a namespaced ``Role``:

.. code:: yaml

   kind: Role
   apiVersion: rbac.authorization.k8s.io/v1
   metadata:
     name: backed-1-developer
     namespace: backend-1
   rules:
   - apiGroups: [""]
     resources: ["pods/log", "events"]
     verbs: ["get", "list"]
   - apiGroups: [""]
     resources: ["pods", "replicationcontrollers"]
     verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
   - apiGroups: [""]
     resources: ["pods/exec"]
     verbs: ["create"]
   - apiGroups: [""]
     resources: ["configmaps", "secrets", "services"]
     verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
   - apiGroups: ["extensions", "apps"]
     resources: ["deployments", "statefulsets"]
     verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]
   - apiGroups: ["apps"]
     resources: ["replicasets"]
     verbs: ["get", "list", "update", "patch", "delete", "create", "watch"]
   - apiGroups: ["networking.k8s.io", "extensions"]
     resources: ["ingresses", "ingressclasses"]
     verbs: ["get", "list", "watch", "create", "update", "patch", "delete"]

This role allows delevopers in the group ``backend-1`` to modify all the
namespaced resources in the ``backend-1`` namespace.
Some resources in kubernetes are cluster wide resources. For these resources
we create an explicit ``ClusterRole``.

.. code:: yaml

   ---
   kind: ClusterRole
   apiVersion: rbac.authorization.k8s.io/v1
   metadata:
     name: system-reader
   rules:
   - apiGroups: [""]
     resources: ["pods/log", "events"]
     verbs: ["get", "list"]
   - apiGroups: [""]
     resources: ["pods"]
     verbs: ["get", "list"]
   - apiGroups: [""]
     resources: ["pods/exec"]
     verbs: ["create"]
   - apiGroups: [""]
     resources: ["secrets"]
     verbs: ["list"]
   - apiGroups: [""]
     resources: ["configmaps", "services"]
     verbs: ["get", "list"]
   - apiGroups: ["extensions", "apps"]
     resources: ["deployments"]
     verbs: ["get", "list", "watch", "update"]
   - apiGroups: ["extensions"]
     resources: ["replicasets"]
     verbs: ["get", "list"]
   - apiGroups: ["storage.k8s.io"]
     resources: ["storageclasses", "volumeattachments"]
     verbs: ["get", "list", "watch"]
   - apiGroups: [""]
     resources: ["persistentvolumeclaims", "persistentvolumes"]
     verbs: ["list", "get", "create", "update", "watch", "delete"]
   - apiGroups: [""]
     resources:
       - componentstatuses
       - events
       - endpoints
       - namespaces
       - nodes
       - resourcequotas
       - services
     verbs: ["get", "watch", "list"]
   - nonResourceURLs: ["*"]
     verbs: ["get", "watch", "list"]


The in order to have an effect we need to create a binding between a ``Role``
or a ``ClusterRole`` to a certain user or group. For the namespaced role
we create ``RoleBinding``:

.. code:: yaml

   ---
   kind: RoleBinding
   apiVersion: rbac.authorization.k8s.io/v1beta1
   metadata:
     name: backend-1
     namespace: backend-1
   subjects:
   # This can also be per user
   # - kind: User
   #   name: david
   #   apiGroup: rbac.authorization.k8s.io
   - kind: Group
     name: backend-1
     apiGroup: rbac.authorization.k8s.io
   roleRef:
     kind: Role
     name: backed-1-developer
     apiGroup: rbac.authorization.k8s.io


We can apply both with::

   $ k create namepace backend-1
   namespace/backend-1 created
   $ k apply -f rbac/role-backend-1.yml
   role.rbac.authorization.k8s.io/backed-1-developer created
   Warning: rbac.authorization.k8s.io/v1beta1 RoleBinding is deprecated in v1.17+, unavailable in v1.22+; use rbac.authorization.k8s.io/v1 RoleBinding
   rolebinding.rbac.authorization.k8s.io/backend-1 created

To check that this work, we switch context and pretend we are no longer admins::

   $ k config use-context oz-alt-shift.digital
   Switched to context "oz-alt-shift.digital".

   $ k auth can-i get pods
   no

   $ k auth can-i get pods -n backend-1
   yes

This works! I can list pods in the ``backend-1`` namespace but not in the
default namespace. To get a quick overview on all my capablities, I can use
the `rbac-access plugin <https://github.com/corneliusweig/rakkess>`_:

.. image:: _static/img/access-matrix.png


We apply the ``ClusterRoleBindings`` and check that it works::

   $ k --context=admin-asd-context apply -f examples/rbac/role-system-reader.yml
   clusterrole.rbac.authorization.k8s.io/system-reader created
   clusterrolebinding.rbac.authorization.k8s.io/sys-reader-backend-1 created
   k access-matrix   -n backend-1 | grep ✔
   configmaps                                      ✔     ✔       ✔       ✔
   deployments.apps                                ✔     ✔       ✔       ✔
   endpoints                                       ✔     ✖       ✖       ✖
   events                                          ✔     ✖       ✖       ✖
   ingresses.extensions                            ✔     ✔       ✔       ✔
   ingresses.networking.k8s.io                     ✔     ✔       ✔       ✔
   persistentvolumeclaims                          ✔     ✔       ✔       ✔
   pods                                            ✔     ✔       ✔       ✔
   replicasets.apps                                ✔     ✔       ✔       ✔
   replicationcontrollers                          ✔     ✔       ✔       ✔
   resourcequotas                                  ✔     ✖       ✖       ✖
   secrets                                         ✔     ✔       ✔       ✔
   services                                        ✔     ✔       ✔       ✔
   statefulsets.apps                               ✔     ✔       ✔       ✔


Users in the ``backend-1`` group can now view ``events``, ``endpoints``,
``resourcequotas``, and ``persistentvolumeclaims``.

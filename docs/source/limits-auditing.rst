Limits, Linting, Auditing
=========================

Having monitoring and alerting is great. However, monitoring and alerting
happen only after problems already occurred.

We need to add a few gate keepers in order to guarantee everyone plays fair
in the shared cluster.

Specifying Namespace Limits
---------------------------

Namespace that do not specify `default Memory Requests and Limits <https://kubernetes.io/docs/tasks/administer-cluster/manage-resources/memory-default-namespace/>`_
are guaranteed to break the cluster sooner or later.
This can happen when someone deploys a container that runs
``SELECT * FROM HUGE_TABLE`` without limits,
as the `node memory will choke <https://youtu.be/GQMx9Aa0_ZU?t=1376>`_.
As a result, the kubelet will stop responding in a fair time, which will cause
the kubernetes scheduler to evict the node, and restart that offensive
container in another node. Eventually, if not attended quickly, this will lead
to a complete cluster outage.

Another problem that can happen is that Pods can suffer from poor performance
due to `lack of CPU resources <https://kubernetes.io/docs/tasks/configure-pod-container/assign-cpu-resource/#if-you-do-not-specify-a-cpu-limit>`_.
This cascades as various errors —
Readiness probe failures, Container stalls, Network disconnections and timeouts
within service calls — all in all leading to reduced latency and increased
error rates.

To avoid this we add limits to all namespaces. These limits are applied to
workloads that do not explicitly specify them.

CPU and Memory Limit requests specify the lower and upper boundaries:

.. code:: yaml

   apiVersion: v1
   kind: LimitRange
   metadata:
     name: cpu-mem-limit-range
   spec:
     limits:
     - default:
         cpu: 1
         memory: 512Mi
       defaultRequest:
         cpu: 0.5
         memory: 256Mi
       type: Container

We apply the manifest to the `backend-1` manifest::

   $ k apply -f examples/limits/cpu-mem-limits.yml -n backend-1
   limitrange/cpu-mem-limit-range created

Specifying Namespace Quotas
---------------------------

Specifying limits for a single `Pod` is still not enough to prevent resource
exhaustion, since one can deploy many `Pods` each requiring a little amount
of resource, all together exhausting the cluster resources.
To avoid this situation, we create a
`total limit of resources <https://kubernetes.io/docs/tasks/administer-cluster/manage-resources/quota-memory-cpu-namespace>`_
for every namespace:

.. code:: yaml

   apiVersion: v1
   kind: ResourceQuota
   metadata:
     name: mem-cpu-quota
   spec:
     hard:
       requests.cpu: "2"
       requests.memory: 2Gi
       limits.cpu: "4"
       limits.memory: 4Gi

To apply this ``ResourceQuota``::

   $ k apply -f examples/limits/cpu-mem-quota.yml -n backend-1
   resourcequota/mem-cpu-quota created

Linting
-------

Kubernetes' manifestss' have a lot of knobs to turn, and a result, there is lot
that can be done wrong. `kubectl` does not have a built it command to vet our
desired change. It only tells if the YAML structure is wrong or not.
There are a few command line tools which can be used to check and analyze
Kubernetes manifest before we apply changes to the cluster.

`kube-score`
^^^^^^^^^^^^

`kube-score` also available as a plugin for `kubectl` will run a large set of
tests to audit kubernetes manifest. It will exist with an error status if
the tests fail, and as such we can include it in a deployment pipeline before
applying changes to the cluster.

.. image:: _static/img/kube-score.png

An alternative tool to vet your resources is `Popeye CLI <https://github.com/derailed/popeye>`_.

Auditing with Admimission Controllers
-------------------------------------

An admission controller is:

    In a nutshell, Kubernetes admission controllers are plugins that govern
    and enforce how the cluster is used. They can be thought of as a gatekeeper
    that intercept (authenticated) API requests and may change the request
    object or deny the request altogether. The admission control process has
    two phases: the mutating phase is executed first, followed by the
    validating phase. Consequently, admission controllers can act as mutating
    or validating controllers or as a combination of both. For example, the
    ``LimitRanger`` admission controller can augment pods with default
    resource requests and limits (mutating phase), as well as verify that pods
    with explicitly set resource requirements do not exceed the per-namespace
    limits specified in the ``LimitRange`` object (validating phase)`.

The kubernetes API server has quite a few admission controllors enabled by
default. To list them you can use::

   $ kubectl exec -it -n kube-system $(kubectl get pods -n kube-system -l component=kube-apiserver --no-headers -o custom-columns=":metadata.name") -- kube-apiserver --help | grep 'enabled ones'

In fact, the ``LimitRange`` we deployed earlier is being used by a mutating
admission controller, which adds these limits to requests which do not include
them.

We can build our own custom `Admission Controllers <https://github.com/slok/kubewebhook>`_.
However, there is a growing list of open source of admission controllers which
can be deployed to the cluster. A few notable examples:

`kube-image-swapper <https://github.com/estahn/k8s-image-swapper>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Mirror images into your own registry and swap image references automatically.

`Portieris <https://github.com/IBM/portieris>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Portieris is a Kubernetes admission controller for the enforcement of image
security policies. You can create image security policies for each Kubernetes
namespace or at the cluster level, and enforce different rules for different
images.

`OPA - Open Policy Agent <https://www.openpolicyagent.org/>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Heavy weigth policy engine to define rules for RBAC, Resource Limits, Run Time
permissions and more. It uses a DSL called `REGO <https://www.openpolicyagent.org/docs/latest/policy-language/>`
to define custom rules for defining these policies, and it can be used
as an `admission controller <https://www.openpolicyagent.org/docs/latest/kubernetes-tutorial/>`_.


`Polaris <https://github.com/FairwindsOps/polaris>`_
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Runs a variety of checks to ensure that Kubernetes pods and controllers are
configured using best practices, helping you avoid problems in the future.

Similar to Popeye CLI, but observe the whole cluster and has a fancy WebUI to
report the health of the cluster.

Before going to the last part of the workshop, we will deploy ``polaris`` as
a dashboard::

   $ kubectl apply -f https://github.com/fairwindsops/polaris/releases/latest/download/dashboard.yaml
   kubectl port-forward --namespace polaris svc/polaris-dashboard 8080:80

To no surprise, the dashboard reports quite a few issues in the resoures
deployed to the cluster. In the next part of the work shop we will build
a deployment pipeline to fix some of these issues.

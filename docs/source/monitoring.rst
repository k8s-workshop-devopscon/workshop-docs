Monitoring
==========

To have a better understand of what happens in the cluster at real time, we
deploy ``Prometheus`` and ``Grafana`` with the
`Prometheus Operator <https://prometheus-operator.dev/>`_.

For the purpose of storing the metrics data collected from out cluster we need
to add persistent storage capabilities in our cluster. For testing and
evaluation purposes we can use the local storage provisioner.

kustomize
---------

Before we apply the manifests for the storage provisioner and Prometheus, we
need to install kustomize.
Follow `the instructions <https://kubectl.docs.kubernetes.io/installation/kustomize/binaries/>`_
to install the latest version of `kustomize`.

Local Storage Class
-------------------

First we apply the local-storage class::

   $ kustomize build manifests/local-storage | k apply -f -
   storageclass.storage.k8s.io/local-storage created

   $ k get sc
   NAME            PROVISIONER                    RECLAIMPOLICY   VOLUMEBINDINGMODE      ALLOWVOLUMEEXPANSION   AGE
   local-storage   kubernetes.io/no-provisioner   Delete          WaitForFirstConsumer   false                  57s


Prometheus Operator
-------------------

Deploy the monitoring stack with::

   $ kustomize build manifests/monitoring | k apply -f -
   namespace/monitoring created
   customresourcedefinition.apiextensions.k8s.io/alertmanagerconfigs.monitoring.coreos.com created
   customresourcedefinition.apiextensions.k8s.io/alertmanagers.monitoring.coreos.com created
   customresourcedefinition.apiextensions.k8s.io/podmonitors.monitoring.coreos.com created
   customresourcedefinition.apiextensions.k8s.io/probes.monitoring.coreos.com created
   customresourcedefinition.apiextensions.k8s.io/prometheuses.monitoring.coreos.com created
   customresourcedefinition.apiextensions.k8s.io/prometheusrules.monitoring.coreos.com created
   customresourcedefinition.apiextensions.k8s.io/servicemonitors.monitoring.coreos.com created
   ..

Repeat twice in case of erros.

This stack is deployed with a retention time of 2 days, in order to limit the
amount of data stored in the local-volumes. For longer retention times, one
should use a more appropriate storage class (in OpenStack:
Cinder block volumes).

Services in the monitoring namespace
------------------------------------

``Prometheus Operator`` deploys a few services we can access through a Web UI.
To access these we can use ``kubectl port-forward``.

List all the services::

  $ k get svc -n monitoring
  NAME                    TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)                      AGE
  alertmanager-main       ClusterIP   10.96.162.84     <none>        9093/TCP                     11m
  alertmanager-operated   ClusterIP   None             <none>        9093/TCP,9094/TCP,9094/UDP   11m
  grafana                 ClusterIP   10.108.216.196   <none>        3000/TCP                     11m
  kube-state-metrics      ClusterIP   None             <none>        8080/TCP,8081/TCP            11m
  node-exporter           ClusterIP   None             <none>        9100/TCP                     11m
  prometheus-k8s          ClusterIP   10.105.65.80     <none>        9090/TCP                     11m
  prometheus-operated     ClusterIP   None             <none>        9090/TCP                     11m
  prometheus-operator     ClusterIP   None             <none>        8080/TCP                     11m

The relevant services are ``alertmanager-main``, ``grafana`` and
``prometheus-k8s``. To view any of them::

  $ k -n monitoring port-forward svc/<service-name> <localport>:<remoteport>

Then open your browser in ``http://localhost:localport``.

Bootstraping a cluster with `kubeadm`
=====================================

A short note about a kubernetes cluster design
----------------------------------------------

Usually, a managed kubernetes cluster will include multiple masters
and a cloud load balancer in front of them. This design has the benefit
of being able to handle failures of one master and handling many requests
to the API server (100s of nodes ...).

.. image:: _static/img/ha-lb-cluster.png

However, this design is costly, and is suitable for a public cloud or in a
case when one has a dedicated hardware loadbalancer.

An alternative desigyn, which can also be used on VPS machines, or bare metal
nodes, is to create a cluster with an edge master node which also serves
as a loadbalancer.

.. image:: _static/img/single-master.png


This design has one caveat: if the master node fails, services are not
available from outside the cluster. If however you restore your LB
functionallity, all services will continue to work as nothing happend.
However, the cluster is in 'ReadOnly' mode. Since, in the absense of a
Kubernetes master and API server you will not be able to change software in
the cluster.
To restore the Kubernetes API functionallilty you will have to restart
the Kubernetes cluster componentes with all the SSL keys and restore
the state of the cluster by restoring a snapshot of etcd.

``kubeadm`` bootstrapping
-------------------------

We use ``kubeadm`` to bootstrap a kuberentes master server on a virtual
machine. Note that the virtual machines and security groups and all
other infrastructure resources have already been created.
(This is out of scope for this workshop).

.. code-block:: bash

   $ kubeadm --init --config kubeadm-config.yml

This command generates a lot of output and in the end it will show a code
snippet that shows how you can join worker nodes to the cluster.

.. code-block:: bash

  To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

  You should now deploy a pod network to the cluster.
  Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

  Then you can join any number of worker nodes by running the following on each as root:

  kubeadm join 10.1.0.124:6443 --token 168fd2.6okup1qv4e52w6oe \
    --discovery-token-ca-cert-hash sha256:bf7d931b49c4fe153d44398942f5605c8ff242ddd418723db53de29468453362
  root@demo:~#


Let's examine the configuration file:

.. code-block:: yaml

    apiVersion: kubeadm.k8s.io/v1beta2
    kind: ClusterConfiguration
    clusterName: meh
    kubernetesVersion: v1.19.8
    networking:
      podSubnet: 192.168.0.0/16
    scheduler: {}
    ---
    apiVersion: kubeadm.k8s.io/v1beta2
    kind: InitConfiguration
    bootstrapTokens:
    - groups:
      - system:bootstrappers:kubeadm:default-node-token
      token: 168fd2.6okup1qv4e52w6oe
      ttl: 24h0m0s
      usages:
      - signing
      - authentication
    ---
    apiVersion: kubelet.config.k8s.io/v1beta1
    kind: KubeletConfiguration
    serverTLSBootstrap: true

As you can see, there are a few buttons to toggle. The full `documentation for
kubeadm configuration file <https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/control-plane-flags/>`_ explains
what other options are available.

`kubectl` configuration file
----------------------------

`kubeadm` bootstraps a kubernetes cluster and creates a user with admin
priveleges for us. The configuration file in `/etc/kubernetes/admin.conf`
should only be used for creating other users!
Think of working with this file as login to your workstation as `root`
(or Adminstrator if you use Windows based Operating System).

Applying a CNI network plugin
-----------------------------
Before we apply the network plugin the Pods distributed among the nodes
cannot communicate with each other. And as such the master and other joined
nodes will node be considered as ready.
Here, we can see that the master has already some pods running - these are
static pods deployed in `/etc/kubernetes/manifests`. However, `coredns-*` is
still not scheduled since there is no master ready.

.. code:: bash

   root@demo:~# kubectl get pods -n kube-system --kubeconfig=/etc/kubernetes/admin.conf
   NAME                           READY   STATUS    RESTARTS   AGE
   coredns-f9fd979d6-6dqbv        0/1     Pending   0          34m
   coredns-f9fd979d6-m6h5h        0/1     Pending   0          34m
   etcd-demo                      1/1     Running   0          35m
   kube-apiserver-demo            1/1     Running   0          35m
   kube-controller-manager-demo   1/1     Running   0          35m
   kube-proxy-jlq52               1/1     Running   0          34m
   kube-scheduler-demo            1/1     Running   0          35m

   root@demo:~# kubectl get nodes -n kube-system --kubeconfig=/etc/kubernetes/admin.conf
   NAME   STATUS     ROLES    AGE   VERSION
   demo   NotReady   master   40m   v1.19.8

After we apply the calico CNI plugin this chages:

.. code:: bash

   root@demo:~# kubectl apply -f calico.yaml --kubeconfig=/etc/kubernetes/admin.conf
   configmap/calico-config created
   customresourcedefinition.apiextensions.k8s.io/bgpconfigurations.crd.projectcalico.org created
   customresourcedefinition.apiextensions.k8s.io/bgppeers.crd.projectcalico.org created
   customresourcedefinition.apiextensions.k8s.io/blockaffinities.crd.projectcalico.org created
   customresourcedefinition.apiextensions.k8s.io/clusterinformations.crd.projectcalico.org created
   customresourcedefinition.apiextensions.k8s.io/felixconfigurations.crd.projectcalico.org created
   customresourcedefinition.apiextensions.k8s.io/globalnetworkpolicies.crd.projectcalico.org created
   customresourcedefinition.apiextensions.k8s.io/globalnetworksets.crd.projectcalico.org created
   customresourcedefinition.apiextensions.k8s.io/hostendpoints.crd.projectcalico.org created
   customresourcedefinition.apiextensions.k8s.io/ipamblocks.crd.projectcalico.org created
   customresourcedefinition.apiextensions.k8s.io/ipamconfigs.crd.projectcalico.org created
   customresourcedefinition.apiextensions.k8s.io/ipamhandles.crd.projectcalico.org created
   customresourcedefinition.apiextensions.k8s.io/ippools.crd.projectcalico.org created
   customresourcedefinition.apiextensions.k8s.io/kubecontrollersconfigurations.crd.projectcalico.org created
   customresourcedefinition.apiextensions.k8s.io/networkpolicies.crd.projectcalico.org created
   customresourcedefinition.apiextensions.k8s.io/networksets.crd.projectcalico.org created
   clusterrole.rbac.authorization.k8s.io/calico-kube-controllers created
   clusterrolebinding.rbac.authorization.k8s.io/calico-kube-controllers created
   clusterrole.rbac.authorization.k8s.io/calico-node created
   clusterrolebinding.rbac.authorization.k8s.io/calico-node created
   daemonset.apps/calico-node created
   serviceaccount/calico-node created
   deployment.apps/calico-kube-controllers created
   serviceaccount/calico-kube-controllers created
   poddisruptionbudget.policy/calico-kube-controllers created
   root@demo:~# kubectl get nodes -n kube-system --kubeconfig=/etc/kubernetes/admin.conf
   NAME   STATUS   ROLES    AGE   VERSION
   demo   Ready    master   42m   v1.19.8

   root@demo:~# kubectl get pod -n kube-system --kubeconfig=/etc/kubernetes/admin.conf | grep core
   coredns-f9fd979d6-6dqbv                    1/1     Running   0          48m
   coredns-f9fd979d6-m6h5h                    1/1     Running   0          48m


Adding worker nodes
-------------------
Using SSH
+++++++++

SSH to each worker node we want to add to the cluster and execute the command
that `kubeadm` printed for us.

.. code:: bash

   ubuntu@demo:~$ ssh node-1 sudo kubeadm join 10.1.0.124:6443 --token 168fd2.6okup1qv4e52w6oe \
   >   --discovery-token-ca-cert-hash sha256:bf7d931b49c4fe153d44398942f5605c8ff242ddd418723db53de29468453362
   [preflight] Running pre-flight checks
   [preflight] Reading configuration from the cluster...
   [preflight] FYI: You can look at this config file with 'kubectl -n kube-system get cm kubeadm-config -oyaml'
   [kubelet-start] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
   [kubelet-start] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
   [kubelet-start] Starting the kubelet
   [kubelet-start] Waiting for the kubelet to perform the TLS Bootstrap...

   This node has joined the cluster:
   * Certificate signing request was sent to apiserver and a response was received.
   * The Kubelet was informed of the new secure connection details.

   Run 'kubectl get nodes' on the control-plane to see this node join the cluster.

   ubuntu@demo:~$ sudo kubectl --kubeconfig /etc/kubernetes/admin.config get nodes
   error: stat /etc/kubernetes/admin.config: no such file or directory
   ubuntu@demo:~$ sudo kubectl --kubeconfig /etc/kubernetes/admin.conf get nodes
   NAME     STATUS   ROLES    AGE   VERSION
   demo     Ready    master   52m   v1.19.8
   node-1   Ready    <none>   34s   v1.19.8


Using `user-data`
+++++++++++++++++

Alternatively, we can use `user-data` to run the command on startup
on many cloud providers.

.. image:: _static/img/cloud-init-openstack-horizon.png

.. code:: bash

   $ sudo kubectl --kubeconfig /etc/kubernetes/admin.conf get nodes
   NAME     STATUS     ROLES    AGE     VERSION
   demo     Ready      master   57m     v1.19.8
   node-1   Ready      <none>   5m36s   v1.19.8
   node-2   NotReady   <none>   22s     v1.19.8

   ubuntu@demo:~$ sudo kubectl --kubeconfig /etc/kubernetes/admin.conf get nodes
   NAME     STATUS   ROLES    AGE     VERSION
   demo     Ready    master   58m     v1.19.8
   node-1   Ready    <none>   6m16s   v1.19.8
   node-2   Ready    <none>   62s     v1.19.8
